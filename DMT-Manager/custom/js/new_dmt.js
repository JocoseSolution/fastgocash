﻿var regmobile = "";

//function NewRegistration() {
//    $("#OTPSectionForm").addClass("hidden");
//    $("#RegistSectionForm").removeClass("hidden");
//    $("#RegistMargin").removeAttr("style").css("margin", "3% auto");
//    $("#txtSenderMobileNo").val("");
//    $("#moberrormsg").html("").removeAttr("style");

//    ResetRegistrationForm();
//    $(".remitterreg").click();
//}

function RemitterMobileSearch() {
    $("#TokenExpired").html("");
    ResetRegistrationForm();
    var thisbutton = $("#btnRemitterMobileSearch");
    if (CheckFocusBlankValidation("txtSenderMobileNo")) return !1;
    var mobileno = $("#txtSenderMobileNo").val();

    if (mobileno.length == 10) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/dmt-manager/senderindex.aspx/remittermobilesearch",
            data: '{mobileno: ' + JSON.stringify(mobileno) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d.length > 0) {
                    if (data.d[0] == "success") { window.location.href = data.d[1]; }
                    else if (data.d[0] == "otpsent") {
                        $("#txtSenderMobileNo").val("");
                        $("#otperrormessage").html("");
                        regmobile = data.d[1];
                        $("#hdnRegRemtId").val(data.d[2]);

                        $("#RegistSectionForm").addClass("hidden");
                        $("#OTPSectionForm").removeClass("hidden");
                        $("#RegOtpHeading").html("Sender already registered, Please verify sender.<br/>Otp sent successfully to your mobile number.");
                        $("#RegistMargin").removeAttr("style").css("margin", "8% auto");
                        $(".remitterreg").click();
                        $(thisbutton).html("Search");
                        StartCountDown(30);
                    }
                    else if (data.d[0] == "registration") {
                        ResetRegistrationForm();
                        $("#txtRegMobileNo").val(mobileno);
                        $(".remitterreg").click();
                        $(thisbutton).html("Search");
                        $("#txtSenderMobileNo").val("");
                    }
                    else if (data.d[0] == "error") {
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Failed !", "#ff414d", data.d[1], "#ff414d");
                        $(thisbutton).html("Search");
                    }
                    else if (data.d[0] == "reload") { window.location.reload(); }
                }
                else {
                    $("#TokenExpired").html("Error occured, Token expired !");
                }
                $(thisbutton).html("Search");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
    else {
        ShowMessagePopup("<i class='fa fa-exclamation-triangle' style='color:#ff414d;' aria-hidden='true'></i>&nbsp;Failed !", "#ff414d", "Mobile number not valid.", "#ff414d");
    }
}

var regirsterremitter = null;
function SubmitRegRemitter() {
    $("#perrormessage").html("");
    $("#otperrormessage").html("");
    var thisbutton = $("#btnRemtrRegistration");


    if (CheckFocusBlankValidation("txtRegMobileNo")) return !1;
    var mobile = $("#txtRegMobileNo").val();
    if (mobile.length != 10) { $("#txtRegMobileNo").focus(); return false; }
    if (CheckFocusBlankValidation("txtRegFirstName")) return !1;
    if (CheckFocusBlankValidation("txtRegLastName")) return !1;
    if (CheckFocusBlankValidation("txtRegPinCode")) return !1;
    if (CheckFocusBlankValidation("txtCurrLocalAddress")) return !1;

    var firstname = $("#txtRegFirstName").val();
    var lastname = $("#txtRegLastName").val();
    var pincode = $("#txtRegPinCode").val();
    var localadd = $("#txtCurrLocalAddress").val();

    if (mobile.length == 10) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/dmt-manager/senderindex.aspx/RemitterRegistration",
            data: '{mobile: ' + JSON.stringify(mobile) + ',firstname: ' + JSON.stringify(firstname) + ',lastname: ' + JSON.stringify(lastname) + ',pincode: ' + JSON.stringify(pincode) + ',localadd: ' + JSON.stringify(localadd) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != null) {
                    if (data.d[0] == "success") {
                        $("#RegistSectionForm").addClass("hidden");
                        $("#OTPSectionForm").removeClass("hidden");
                        $("#hdnRegRemtId").val(data.d[1]);
                        regirsterremitter = data.d[1];
                        regmobile = mobile;
                        StartCountDown(30);
                    }
                    else if (data.d[0] == "failed") {
                        $("#perrormessage").html(data.d[1]);
                    }
                    else if (data.d[0] == "reload") {
                        window.location.reload();
                    }
                    else {
                        $("#btnregclose").click();
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Error Occurred !", "#d91717", data.d[1], "#d91717");
                    }
                }
                else {
                    $("#perrormessage").html("Error occured, Token expired !");
                }
                $(thisbutton).html("Register");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
    else {
        $("#perrormessage").html("Mobile number should be 10 digits!");
    }
}

function SubmitRegOtpVerification() {
    $("#otperrormessage").html("");
    var thisbutton = $("#btnRegOtpVarification");
    if (CheckFocusBlankValidation("txtRegOtp")) return !1;

    var mobile = regmobile;
    var enteredotp = $("#txtRegOtp").val();
    var hdnremtVal = $("#hdnRegRemtId").val();
    var remitterid = hdnremtVal != "" ? hdnremtVal : regirsterremitter;

    if (mobile != "" && enteredotp != "" && remitterid != "") {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/dmt-manager/senderindex.aspx/RemitterVarification",
            data: '{mobile: ' + JSON.stringify(mobile) + ',remitterid: ' + JSON.stringify(remitterid) + ',otp: ' + JSON.stringify(enteredotp) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != null) {
                    if (data.d[0] == "success") {
                        $("#btnregformclose").click();
                        window.location.href = data.d[1];
                        //ShowMessagePopup("<i class='fa fa-check-circle text-success' aria-hidden='true'></i>&nbsp;Success", "#28a745", "Remitter registration has been successfully completed.", "#28a745");
                    }
                    else if (data.d[0] == "failed") {
                        $("#otperrormessage").html(data.d[1]);
                    }
                    else {
                        $("#btnregformclose").click();
                        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Error Occurred !", "#d91717", data.d[1], "#d91717");
                    }
                }
                $(thisbutton).html("Verify");
                $("#txtRegOtp").val("");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

function ResetRegistrationForm() {
    $("#txtRegMobileNo").val("");
    $("#txtRegFirstName").val("");
    $("#txtRegLastName").val("");
    $("#txtRegPinCode").val("");
    $("#txtRegOtp").val("");
    $("#txtCurrLocalAddress").val("");
    // $("#moberrormsg").html("");
    $("#perrormessage").html("");
}

function ShowMessagePopup(headerHeading, headcolor, bodyMsg, bodycolor) {
    $(".popupopenclass").click();
    $(".popupheading").css("color", headcolor).html(headerHeading);
    $(".popupcontent").css("color", bodycolor).html(bodyMsg);
}

$(document.body).on('click', "#UpdateLocalAddress", function (e) {
    $("#updatemsg").html("");
    $(".updateaddresspopup").click();
});

$("#UpdateAddressClose").click(function () {
    window.location.reload();
});

function UpdateCurrLocalAddress() {
    $("#updatemsg").html("");
    var thisbutton = $("#btnUpdateCurrLocalAddress");
    if (CheckFocusBlankValidation("txtUpdateCurrLocalAddress")) return !1;

    var address = $("#txtUpdateCurrLocalAddress").val();
    if (address != "") {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/SenderDetails.aspx/UpdateLocalAddress",
            data: '{updatedaddress: ' + JSON.stringify(address) + '}',
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data.d[0] == "success") {
                        $("#txtUpdateCurrLocalAddress").val("");
                        $("#updatemsg").html(data.d[1]);
                    }
                    else {
                        $("#updatemsg").html(data.d[1]);
                    }
                }
                $(thisbutton).html("Update Address");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

//==========================================Sender Details Page===========================================================
function GetParameterValues(param) {
    var url = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
    for (var i = 0; i < url.length; i++) { var urlparam = url[i].split('='); if (urlparam[0] == param) { return urlparam[1]; } }
}

$(function () {
    var remitterid = GetParameterValues('sender');
    var remittermob = GetParameterValues('mobile');
    var pagereg = GetParameterValues('page');
    if (pagereg != null) { if (pagereg == "registration") { ResetRegistrationForm(); $(".remitterreg").click(); } }

    //if ((remitterid != null && remitterid != "") && (remittermob != null && remittermob != "")) {
    //    BindRemitterDetails();
    //}
});

//function BindRemitterDetails() {
//    $("#RemitterDetailsSection").html("");

//    $.ajax({
//        type: "Post",
//        contentType: "application/json; charset=utf-8",
//        url: "/DMT-Manager/SenderDetails.aspx/BindRemitterDetails",
//        //data: '{mobileno: ' + JSON.stringify(mobileno) + '}',
//        datatype: "json",
//        success: function (data) {
//            if (data != null) {
//                $("#RemitterDetailsSection").html(data.d[0]);
//                $("#BeneficaryRowDetails").html(data.d[1]);
//            }
//        },
//        failure: function (response) {
//            alert("failed");
//        }
//    });
//}

function AddNewReceiver() {
    $("#txtBenAccountNo").val("");
    $("#txtBenIFSCNo").val("");
    $("#txtBenPerName").val("");
    //$("#txtBenPerMobile").val("");
    $("#benerrormessage").html("");
    $("#bensuccessmessage").html("");
    $(".addbenificiarydetail").click();
    BindAllBank();
}

function SubmitBeneficiaryDetail() {
    $("#benerrormessage").html("");
    $("#bensuccessmessage").html("");

    var thisbutton = $("#btnBenRegistration");

    if (CheckFocusBlankValidation("txtBenAccountNo")) return !1;
    if (BankCheckFocusDropDownBlankValidation("ddlBindBankDrop")) return !1;
    if (CheckFocusBlankValidation("txtBenIFSCNo")) return !1;
    if (CheckFocusBlankValidation("txtBenPerName")) return !1;
    //if (CheckFocusBlankValidation("txtBenPerMobile")) return !1;

    var accountno = $("#txtBenAccountNo").val();
    var bankname = $("#ddlBindBankDrop option:selected").text();
    var ifsccode = $("#txtBenIFSCNo").val();
    var name = $("#txtBenPerName").val();
    //var mobile = $("#txtBenPerMobile").val();

    if (confirm("Are you sure you want to add beneficiary ?")) {
        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/SenderDetails.aspx/BeneficiaryRegistration",
            //data: '{benmobile: ' + JSON.stringify(mobile) + ',accountno: ' + JSON.stringify(accountno) + ',bankname: ' + JSON.stringify(bankname) + ',ifsccode: ' + JSON.stringify(ifsccode) + ',name: ' + JSON.stringify(name) + '}',
            data: '{accountno: ' + JSON.stringify(accountno) + ',bankname: ' + JSON.stringify(bankname) + ',ifsccode: ' + JSON.stringify(ifsccode) + ',name: ' + JSON.stringify(name) + '}',
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data.d[0] == "success") {
                        $("#BeneficaryRowDetails").html("");
                        $("#BeneficaryRowDetails").html(data.d[1]);
                        $("#bensuccessmessage").html("Beneficiary has been added successfully.");
                        ResetBeneficiaryFields();
                        //$('#FormBeneficiary').modal('hide');

                        //ShowMessagePopup("<i class='fa fa-check-circle text-success' aria-hidden='true'></i>&nbsp;Success", "#28a745", "Beneficiary has been added successfully.", "#28a745");
                    }
                    else if (data.d[0] == "failed" || data.d[0] == "error") {
                        $("#benerrormessage").html(data.d[1]);
                    }
                    else if (data.d[0] == "reload") {
                        window.location.reload();
                    }
                }
                $(thisbutton).html("Submit");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

function ResetBeneficiaryFields() {
    $("#txtBenAccountNo").val("");
    $("#txtBenIFSCNo").val("");
    $("#txtBenPerName").val("");
    //$("#txtBenPerMobile").val("");
    $("#typeofifsc").html("");
    $("#txtBenIFSCNo").val("");
    $("#ddlBindBankDrop").val("").change();
}

function DirectTransferMoney(benid) {
    if (benid != null) {
        $("#FundTransDetailBody").html("");
        $("#FundTransDetailFooter").html("");
        $("#FundTransDetailFooter").removeClass("hidden");
        $("#paratranserror").html("");

        var thisbutton = $("#btnMoneyTransfer_" + benid);

        if (CheckFocusBlankValidation("txtTranfAmount_" + benid)) return !1;
        var amount = $("#txtTranfAmount_" + benid).val();

        if (amount != "") {
            $(thisbutton).html("Wait..<i class='fa fa-pulse fa-spinner'></i>");

            $.ajax({
                type: "Post",
                contentType: "application/json; charset=utf-8",
                url: "/DMT-Manager/SenderDetails.aspx/GetFundTransferVeryficationDetail",
                data: '{benid: ' + JSON.stringify(benid) + ',amount: ' + JSON.stringify(amount) + '}',
                datatype: "json",
                success: function (data) {
                    if (data != null) {
                        if (data.d[0] == "success") {
                            $("#FundTransDetailBody").html(data.d[1]);
                            $("#FundTransDetailFooter").html(data.d[2]);
                            $(".VerifyTransferDetails").click();
                        }

                        $("#txtTranfAmount_" + benid).val("");
                    }
                    $(thisbutton).html("Transfer");
                },
                failure: function (response) {
                    alert("failed");
                }
            });

            //if (confirm('Are you sure, want to fund transfer ?')) {
            //    $(thisbutton).html("<i class='fa fa-pulse fa-spinner'></i>");

            //    $.ajax({
            //        type: "Post",
            //        contentType: "application/json; charset=utf-8",
            //        url: "/DMT-Manager/SenderDetails.aspx/FundTransfer",
            //        data: '{benid: ' + JSON.stringify(benid) + ',amount: ' + JSON.stringify(amount) + '}',
            //        datatype: "json",
            //        success: function (data) {
            //            if (data != null) {
            //                if (data.d[0] == "success") {
            //                    ShowMessagePopup("<i class='fa fa-check-circle text-success' aria-hidden='true'></i>&nbsp;Success", "#28a745", data.d[1], "#28a745");
            //                }
            //                else if (data.d[0] == "under_process") {
            //                    ShowMessagePopup("<i class='fa fa-clock text-warning' aria-hidden='true'></i>&nbsp;Pending !", "#ffc107", data.d[1], "#ffc107");
            //                }
            //                else if (data.d[0] == "error") {
            //                    ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Error Occurred !", "#d91717", data.d[1], "#d91717");
            //                }
            //                else if (data.d[0] == "failed") {
            //                    ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Error Occurred !", "#d91717", data.d[1], "#d91717");
            //                }
            //                else if (data.d[0] == "duplicate") {
            //                    ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Error Occurred !", "#d91717", data.d[1], "#d91717");
            //                }

            //                $("#txtTranfAmount_" + benid).val("");
            //            }
            //            $(thisbutton).html("Transfer");
            //        },
            //        failure: function (response) {
            //            alert("failed");
            //        }
            //    });
            //}
        }
    }
}

function IMPSFundTransfer() {
    $("#paratranserror").html("").removeClass("text-danger");
    var benid = $("#btnIMPSTrans").data("benid");
    var amount = $("#btnIMPSTrans").data("transamount");
    var transmode = "1";
    var acceptdeclar = false;

    if ($("#ChkFundTransCheckboxCheck").prop("checked") == true) { acceptdeclar = true; } else {
        $("#paratranserror").html("Please click on checkbox for accept the above declaraction.").addClass("text-danger");;
    }
    if (acceptdeclar) {
        ProcessToFundTransfer(benid, amount, transmode, $("#btnIMPSTrans"), $("#btnNEFTTrans"), "IMPS");
    }
}

function NEFTFundTransfer() {
    $("#paratranserror").html("").removeClass("text-danger");

    var benid = $("#btnIMPSTrans").data("benid");
    var amount = $("#btnIMPSTrans").data("transamount");
    var transmode = "0";
    var acceptdeclar = false;

    if ($("#ChkFundTransCheckboxCheck").prop("checked") == true) { acceptdeclar = true; } else {
        $("#paratranserror").html("Please click on checkbox for accept the above declaraction.").addClass("text-danger");
    }
    if (acceptdeclar) {
        ProcessToFundTransfer(benid, amount, transmode, $("#btnNEFTTrans"), $("#btnIMPSTrans"), "NEFT");
    }
}

function ProcessToFundTransfer(benid, amount, transmode, btnid, disbtnid, btnname) {
    $(btnid).html("Processing..<i class='fa fa-pulse fa-spinner'></i>").prop('disabled', true);
    $(disbtnid).prop('disabled', true);
    $("#paratranserror").html("");
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/SenderDetails.aspx/ProcessToFundTransfer",
        data: '{benid: ' + JSON.stringify(benid) + ',amount: ' + JSON.stringify(amount) + ',transmode: ' + JSON.stringify(transmode) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#FundTransDetailBody").html("");
                    $("#FundTransDetailBody").html(data.d[1]);
                    $("#FundTransDetailFooter").html("");
                    $("#FundTransDetailFooter").addClass("hidden");
                    $("#VerifyTransferModalDialog").removeAttr("style").css("margin", "3% auto");

                    $("#RemitterDetailsSection").html("");
                    $("#RemitterDetailsSection").html(data.d[2]);
                }
                else if (data.d[0] == "lowlimit" || data.d[0] == "nolimit" || data.d[0] == "failed") {
                    $("#paratranserror").html(data.d[1]).addClass("text-danger");
                }
            }
            $(btnid).html(btnname).prop('disabled', false);;
            $(disbtnid).prop('disabled', false);
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

$("#TransHistory").click(function () {
    ClearTransFilter();
    //$("#FundTransRowDetails").html("");
    //BindTransAllDetails();
});

//$("#PayoutTransHistory").click(function () {
//    ClearPayoutTransFilter();
//    //$("#FundTransRowDetails").html("");
//    //BindTransAllDetails();
//});

function BindTransAllDetails() {
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/SenderDetails.aspx/GetTransactionHistory",
        //data: '{benid: ' + JSON.stringify(benid) + ',amount: ' + JSON.stringify(amount) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#FundTransRowDetails").html(data.d[1]);
                }
            }
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

function ShowBenDetails(benid) {
    if (benid != null) {
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/SenderDetails.aspx/GetBeneficiaryDetailById",
            data: '{benid: ' + JSON.stringify(benid) + '}',
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    $(".benificierypopupclass").click();
                    $("#BenBodyContent").html(data.d);
                }
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

function TransFilter() {
    var thisbutton = $("#btnTransFilter");

    var fromdate = $("#txtTransFromDate").val();
    var todate = $("#txtTransToDate").val();
    var trackid = $("#txtTransTrackId").val();
    var filstatus = $("#ddlTransStatus option:selected").val();

    $(thisbutton).html("Search... <i class='fa fa-pulse fa-spinner'></i>");

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/SenderDetails.aspx/GetFilterTransactionHistory",
        data: '{fromdate: ' + JSON.stringify(fromdate) + ',todate: ' + JSON.stringify(todate) + ',trackid: ' + JSON.stringify(trackid) + ',filstatus: ' + JSON.stringify(filstatus) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d[0] == "success") {
                    $("#FundTransRowDetails").html("");

                    $("#FundTransRowDetails").html(data.d[1]);
                }
            }
            $(thisbutton).html("Search");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

//function ClearPayoutTransFilter() {
//    $("#btnPayoutTransFilter").html("Search");

//    $("#txtPayoutTransFromDate").val("");
//    $("#txtPayoutTransToDate").val("");
//    $("#txtPayoutTransTrackId").val("");  

//    $("#PayoutFundTransRowDetails").html("");
//    //BindPayoutTransAllDetails();

//    $("#payoutfilterbtn").click();
//}

function ClearTransFilter() {
    $("#btnTransFilter").html("Search");

    $("#txtTransFromDate").val("");
    $("#txtTransToDate").val("");
    $("#txtTransTrackId").val("");
    $("#ddlTransStatus").prop('selectedIndex', '').change();

    $("#FundTransRowDetails").html("");
    BindTransAllDetails();

    //$("#filterbtn").click();
}

var deletebenid = null;
function DeleteBeneficialRecord(benid) {
    $("#DeleteSuccessfully").addClass("hidden");
    if (benid != null) {
        deletebenid = benid;
        $("#ConfirmBenDelete").removeClass("hidden");
        $("#BenDeleteOtpSent").addClass("hidden");
        $("#txtBenDeleteOtp").val("");
        $("#btnBenDelVarification").html("Verify");
        $("#btnDelConfirmed").html("YES");

        $(".delbendetail").click();
    }
}

function ConfirmDeleteDen() {
    if (deletebenid != null) {
        $("#btnDelConfirmed").html("<i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/SenderDetails.aspx/DeleteBeneficialRecord",
            data: '{benid: ' + JSON.stringify(deletebenid) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != null) {
                    if (data.d[0] == "success") {
                        $("#ConfirmBenDelete").addClass("hidden");
                        $("#BenDeleteOtpSent").removeClass("hidden");
                    }
                    else {
                        $("#ConfirmBenDelete").addClass("hidden");
                        $("#BenDeleteOtpSent").addClass("hidden");
                        $("#bendeletemsg").html("Error Occurred !");
                        $("#DeleteSuccessfully").removeClass("hidden");

                        //$("#BenDelCloseClick").click();
                        //ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", data.d[1], "#d91717");
                    }
                }

                $("#btnDelConfirmed").html("Yes");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

function BenDelVarification() {
    if (deletebenid != null) {
        $("#bendelvariyerror").html("");

        var thisbutton = $("#btnBenDelVarification");

        if (CheckFocusBlankValidation("txtBenDeleteOtp")) return !1;
        var otp = $("#txtBenDeleteOtp").val();

        $(thisbutton).html("Please Wait... <i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/SenderDetails.aspx/DeleteBeneficiaryVarification",
            data: '{benid: ' + JSON.stringify(deletebenid) + ',otp: ' + JSON.stringify(otp) + '}',
            datatype: "json",
            success: function (data) {
                if (data.d != null) {
                    if (data.d[0] == "success") {
                        $("#BeneficaryRowDetails").html("");
                        $("#BeneficaryRowDetails").html(data.d[1]);

                        $("#ConfirmBenDelete").addClass("hidden");
                        $("#BenDeleteOtpSent").addClass("hidden");
                        $("#bendeletemsg").html("Benificiary has been deleted successfully.");
                        $("#DeleteSuccessfully").removeClass("hidden");
                        $("#txtBenDeleteOtp").val("");
                        //$("#BenDelCloseClick").click();
                        //ShowMessagePopup("<i class='fa fa-check-circle text-success' aria-hidden='true'></i>&nbsp;Success", "#28a745", "Beneficiary has been deleted successfully.", "#28a745");
                    }
                    else {
                        $("#bendelvariyerror").html(data.d[1]);
                        //$("#BenDelCloseClick").click();
                        //ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Failed !", "#d91717", data.d[1], "#d91717");
                    }
                }

                $(thisbutton).html("Verify");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

//$("#txtRegMobileNo").keyup(function () {
//    $("#moberrormsg").html("");
//    $("#perrormessage").html("");

//    var mobilenumber = $("#txtRegMobileNo").val();
//    if (mobilenumber.length == 10) {
//        $("#moberrormsg").html("verifying...<i class='fa fa-spinner fa-pulse'></i>").css({ "position": "absolute", "right": "70px", "top": "145px" });
//        $.ajax({
//            type: "Post",
//            contentType: "application/json; charset=utf-8",
//            url: "/DMT-Manager/SenderIndex.aspx/DirectRemitterMobileSearch",
//            data: '{mobileno: ' + JSON.stringify(mobilenumber) + '}',
//            datatype: "json",
//            success: function (data) {
//                if (data != null) {
//                    if (data.d[0] == "success") { window.location.href = data.d[1]; }
//                    else if (data.d[0] == "alreadyreg") {
//                        $("#RegOtpHeading").html("");
//                        $("#otperrormessage").html("");
//                        $("#RegOtpHeading").html("Sender already registered, Please verify sender.<br/>Otp sent successfully to your mobile number.");
//                        $("#RegistMargin").removeAttr("style").css("margin", "8% auto");

//                        $("#RegistSectionForm").addClass("hidden");
//                        $("#OTPSectionForm").removeClass("hidden");
//                        $("#hdnRegRemtId").val(data.d[1]);

//                        regmobile = mobilenumber;
//                        StartCountDown(30);
//                    }
//                    else if (data.d[0] == "invalid") { $("#moberrormsg").html("").removeAttr("style"); $("#moberrormsg").html(data.d[1]); }
//                    else if (data.d[0] == "notfound") { $("#moberrormsg").html("").removeAttr("style"); $("#moberrormsg").html(data.d[1]); }
//                    else if (data.d[0] == "error") { $("#moberrormsg").html(""); $("#perrormessage").html(data.d[1]); }
//                    else { $("#moberrormsg").html("").removeAttr("style"); }
//                }
//            },
//            failure: function (response) { $("#moberrormsg").html("").removeAttr("style"); alert("failed"); }
//        });
//    }
//    else {
//        $("#moberrormsg").html("(10 digits) Please don not use prefix zero (0)");
//    }
//});

function BindAllBank() {
    $("#ProcessBindingBank").removeClass("hidden");
    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/DMT-Manager/SenderDetails.aspx/BindAllBank",
        //data: '{mobileno: ' + JSON.stringify(mobilenumber) + '}',
        datatype: "json",
        success: function (data) {
            if (data != null) {
                if (data.d != "") {
                    $("#ddlBindBankDrop").html("");
                    $("#ddlBindBankDrop").html(data.d);
                }
            }
            $("#ProcessBindingBank").addClass("hidden");
        },
        failure: function (response) { alert("failed"); }
    });
}

$("#ddlBindBankDrop").change(function () {
    $("#typeofifsc").html("");
    $("#txtBenIFSCNo").val("");
    var selectedifsccode = $("#ddlBindBankDrop option:selected").val();
    if (selectedifsccode != null && selectedifsccode != "") {
        $("#typeofifsc").html("universal ifsc code.");
        $("#txtBenIFSCNo").val(selectedifsccode);
    }
});

//$(window).on('load', function () {
//    BindAllBank();
//});

function ResendOTP() {
    $("#otperrormessage").html("");
    $("#btnResendOTP").html("Wait... <i class='fa fa-pulse fa-spinner'></i>");

    $.ajax({
        type: "Post",
        contentType: "application/json; charset=utf-8",
        url: "/dmt-manager/senderindex.aspx/ResendOtpToRemitterMobile",
        data: '{mobileno: ' + JSON.stringify(regmobile) + '}',
        datatype: "json",
        success: function (data) {
            if (data.d != null) {
                if (data.d[0] == "otpsent") {
                    $("#otperrormessage").html("OTP sent.");
                    StartCountDown(30);
                }
                else {
                    $("#otperrormessage").html(data.d[1]);
                }
            }
            $("#btnResendOTP").html("Resend OTP");
        },
        failure: function (response) {
            alert("failed");
        }
    });
}

var _tick = null;
function StartCountDown(startSecond) {
    $("#btnResendOTP").addClass("hidden");
    $("#timersection").removeClass("hidden");

    clearInterval(_tick);
    $("#secRemaing").html("");
    $("#minRemaing").html("");
    $(".strcountdown").removeClass("hidden");
    var remSeconds = startSecond;
    var secondCounters = remSeconds % 60;
    function formarteNumber(number) { if (number < 10) return '0' + number; else return '' + number; }
    function startTick() {
        $("#secRemaing").text(formarteNumber(secondCounters));
        $("#minRemaing").text(formarteNumber(parseInt(remSeconds / 60)));
        if (secondCounters == 0) { secondCounters = 60; }
        _tick = setInterval(function () {
            if (remSeconds > 0) {
                remSeconds = remSeconds - 1;
                secondCounters = secondCounters - 1;
                $("#secRemaing").text(formarteNumber(secondCounters));
                $("#minRemaing").text(formarteNumber(parseInt(remSeconds / 60)));
                if (secondCounters == 0) { secondCounters = 60; }
            }
            else {
                clearInterval(_tick);
                ResetResend();
            }
        }, 1000);
    }
    startTick();
}

function ResetResend() {
    $("#btnResendOTP").removeClass("hidden");
    $("#timersection").addClass("hidden");
}

function GetCheckBeneficiaryName() {
    if (CheckFocusBlankValidation("txtBenAccountNo")) return !1;
    if (BankCheckFocusDropDownBlankValidation("ddlBindBankDrop")) return !1;
    if (CheckFocusBlankValidation("txtBenIFSCNo")) return !1;

    var accountnumber = $("#txtBenAccountNo").val();
    var bankname = $("#ddlBindBankDrop option:selected").text();
    var ifsccode = $("#ddlBindBankDrop option:selected").val();
}

function RefundFaildAmount(transid) {
    var thisbutton = $("#btnRefund_" + transid);

    var amount = thisbutton.data("amount");
    var trackid = thisbutton.data("trackid");
    var reportid = thisbutton.data("reportid");

    if (reportid != "") {
        $(thisbutton).html("<i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/SenderDetails.aspx/ProcessToReFund",
            data: '{transid: ' + JSON.stringify(transid) + ',amount: ' + JSON.stringify(amount) + ',trackid: ' + JSON.stringify(trackid) + ',reportid: ' + JSON.stringify(reportid) + '}',
            datatype: "json",
            success: function (data) {
                $("#FundTransRowDetails").html("");
                $("#FundTransRowDetails").html(data.d[1]);

                if (data.d[0] == "false") {
                    ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Error Occurred !", "#d91717", "This Transaction is not for refund !", "#d91717");
                }
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
    else {
        ShowMessagePopup("<i class='fa fa-exclamation-triangle text-danger' aria-hidden='true'></i>&nbsp;Error Occurred !", "#d91717", "This Transaction is not for refund !", "#d91717");
    }
}

function CheckTransStatus(transid) {
    var thisbutton = $("#btnCheckStatus_" + transid);

    var orderid = thisbutton.data("orderid");
    var amount = thisbutton.data("amount");
    var trackid = thisbutton.data("trackid");
    var reportid = thisbutton.data("reportid");
    if (orderid != "") {
        $(thisbutton).html("processing...<i class='fa fa-pulse fa-spinner' style='padding: 0rem 0.3rem;font-size: 12px;'></i>");
        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/SenderDetails.aspx/CheckTransStatus",
            data: '{transid: ' + JSON.stringify(transid) + ',fdate: ' + JSON.stringify($("#txtTransFromDate").val()) + ',tdate: ' + JSON.stringify($("#txtTransToDate").val()) + ',strackid: ' + JSON.stringify($("#txtTransTrackId").val()) + ',status: ' + JSON.stringify($("#ddlTransStatus option:selected").val()) + ',trackid: ' + JSON.stringify(trackid) + ',orderid: ' + JSON.stringify(orderid) + '}',
            datatype: "json",
            success: function (data) {
                $(".transstatuspopupclass").click();
                $("#TransStatusBodyContent").html(data.d[1]);

                $("#FundTransRowDetails").html("");
                $("#FundTransRowDetails").html(data.d[2]);

                $(thisbutton).html("Check Status");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}

//---------------------------Payout---------------------------------
function GetBeneficiaryNamePayout() {
    if (CheckFocusBlankValidation("txtBenAccountNo")) return !1;
    if (BankCheckFocusDropDownBlankValidation("ddlBindBankDrop")) return !1;
    if (CheckFocusBlankValidation("txtBenIFSCNo")) return !1;

    var accountno = $("#txtBenAccountNo").val();
    var bankname = $("#ddlBindBankDrop option:selected").text();
    var transfertype = $("#ddlBindBankDrop option:selected").data("transfertype");
    var ifsccode = $("#txtBenIFSCNo").val();
    var paytype = "getname";

    if (confirm("Are you sure you want to get beneficiary detail, It will charge ₹ 3 ?")) {
        $("#CheckAccountDetailPayout").html("featching...<i class='fa fa-pulse fa-spinner'></i>");

        $.ajax({
            type: "Post",
            contentType: "application/json; charset=utf-8",
            url: "/DMT-Manager/SenderDetails.aspx/GetBeneficiaryNamePayout",
            data: '{accountno: ' + JSON.stringify(accountno) + ',transtype: ' + JSON.stringify(transfertype) + ',ifsccode: ' + JSON.stringify(ifsccode) + ',bankname:' + JSON.stringify(bankname) + ',paytype:' + JSON.stringify(paytype) + '}',
            datatype: "json",
            success: function (data) {
                if (data != null) {
                    if (data.d[0] == "success") {
                        $("#txtBenPerName").val(data.d[1]);
                    }
                    else if (data.d[0] == "reload") {
                        window.location.reload();
                    }
                    else {
                        $("#benerrormessage").html("Error: " + data.d[1]);
                    }
                }
                $("#CheckAccountDetailPayout").html("Get Beneficiary Name");
            },
            failure: function (response) {
                alert("failed");
            }
        });
    }
}