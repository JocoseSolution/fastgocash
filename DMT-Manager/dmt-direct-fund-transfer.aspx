﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterForHome.master" AutoEventWireup="true" CodeFile="dmt-direct-fund-transfer.aspx.cs" Inherits="DMT_Manager_dmt_direct_fund_transfer" %>

<%@ Register Src="~/DMT-Manager/User_Control/dmtdirect/uc-dmt-direct-fund-transfer.ascx" TagPrefix="uc1" TagName="ucdmtdirectfundtransfer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="custom/css/dmtdirect.css" rel="stylesheet" />
    <link href="custom/css/validationcss.css" rel="stylesheet" />
    <link href="custom/css/datetime.css" rel="stylesheet" />
    <style>
        .upper_letter {
            text-transform: uppercase !important;
        }
    </style>    

    <div class="container">
        <div class="row">
            <div id="RemitterDetailsSection" class="col-sm-12" style="border: 1px solid #ccc; padding: 12px; background: #fff; margin-top: 12px; margin-bottom: 12px;">
                <%=PayoutRemitterDetail %>
            </div>
            <%--saurav code--%>
            <%if (ShowKYCForm)
                { %>
            <div class="col-sm-12" style="margin-top: 25px; border: 1px solid #ccc; padding: 12px; background: #fff; margin-top: 12px; margin-bottom: 12px;">
                <h3 class="text-center" style="color: #ff414d;">Complete Kyc Form</h3>
                <div class="panel panel-default">
                    <div class="panel-heading">Sender Registration</div>
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="row form-group">
                                <div class="col-sm-12 text-center">
                                    <label style="font-size: 20px; color: #ff414d;">Mobile Number : <%=Mobile %></label>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6 form-validation">
                                    <label style="font-size: 16px; color: #ff414d;">First Name</label>
                                    <asp:TextBox ID="txtPayoutFirstName" runat="server" class="form-control padding5px" placeholder="First Name"></asp:TextBox>
                                </div>
                                <div class="col-sm-6 form-validation">
                                    <label style="font-size: 16px; color: #ff414d;">Last Name</label>
                                    <asp:TextBox ID="txtPayoutLastName" runat="server" class="form-control padding5px" placeholder="Last Name"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6 form-validation">
                                    <label style="font-size: 16px; color: #ff414d;">Address as per ID proof</label>
                                    <asp:TextBox ID="txtPayoutAddress" runat="server" class="form-control padding5px" TextMode="MultiLine" Rows="3" placeholder="Address As Per ID Proof"></asp:TextBox>
                                </div>
                                <div class="col-sm-6">
                                    <div class="row">
                                        <div class="col-sm-6 form-validation">
                                            <label style="font-size: 16px; color: #ff414d;">Gender</label>
                                            <asp:DropDownList class="form-control padding5px" ID="ddlPayoutGender" runat="server">
                                                <asp:ListItem Value="">Gender</asp:ListItem>
                                                <asp:ListItem Value="male">Male</asp:ListItem>
                                                <asp:ListItem Value="female">Female</asp:ListItem>
                                            </asp:DropDownList>
                                        </div>
                                        <div class="col-sm-6 form-validation">
                                            <label style="font-size: 16px; color: #ff414d;">DOB</label>
                                            <asp:TextBox ID="txtPayoutDOB" runat="server" class="form-control commonDate padding5px" placeholder="DD/MM/YYYY"></asp:TextBox>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading">ID Proof</div>
                    <div class="panel-body">
                        <div class="col-sm-12">
                            <div class="row form-group">
                                <div class="col-sm-6 form-validation">
                                    <label style="font-size: 16px; color: #ff414d;">Proof</label>
                                    <asp:DropDownList class="form-control padding5px" ID="ddlPayoutProof" runat="server">
                                        <asp:ListItem Value="">Select</asp:ListItem>
                                        <asp:ListItem Value="aadharcard">Aadhar Card</asp:ListItem>
                                        <asp:ListItem Value="votaridcard">Votar Id Card</asp:ListItem>
                                        <asp:ListItem Value="passport">Passport</asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                                <div class="col-sm-6 form-validation">
                                    <label style="font-size: 16px; color: #ff414d;">Number</label>
                                    <asp:TextBox ID="txtProofNumber" runat="server" class="form-control padding5px" placeholder="Selected Proof Id number"></asp:TextBox>
                                </div>
                            </div>
                            <div class="row form-group">
                                <div class="col-sm-6 form-validation">
                                    <label style="font-size: 16px; color: #ff414d;">Proof Front Image</label>
                                    <br />
                                    <asp:FileUpload ID="fluPayoutFrontImg" runat="server" />
                                </div>
                                <div class="col-sm-6 form-validation">
                                    <label style="font-size: 16px; color: #ff414d;">Proof Back Image</label>
                                    <br />
                                    <asp:FileUpload ID="fluPayoutBackImg" runat="server" />
                                </div>
                            </div>
                            <br />
                            <div class="row form-group">
                                <div class="col-sm-12 form-validation">
                                    <label style="color: #ff414d;">
                                        <asp:CheckBox class="custom-checkbox" ID="chkPayoutDeclaration" runat="server" />
                                        By Clicking the checkbox I accept the below declaration</label>
                                </div>
                                <div class="col-sm-12 form-group">
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <p style="margin: 0 0px;">1. The Id document has to be marked as "orginal seen and verified"</p>
                                            <p style="margin: 0 0px;">2. Remitter needs to put this stamp on the id document copy.</p>
                                            <p style="margin: 0 0px;">3. The Id document needs to be posted to BAREILLY BAREILLY UTTAR PRADESH, UTTAR PRADESH, 243001 with in 30 days.</p>
                                        </div>
                                        <div class="col-sm-6">
                                            <p style="margin: 0 0px;">4. I here by declare, that the information filled herein above is correct and as per the information provided by the customer.</p>
                                            <p style="margin: 0 0px;">2. The customer has approached me in person for the remittance service.</p>
                                            <p style="margin: 0 0px;">3. The customer ID document have been verified in original.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <asp:Button ID="btnPayoutSubmitKYC" runat="server" class="btn btn-primary btn-block" Style="width: 25%; margin-left: auto; margin-right: auto;" OnClick="PayoutSubmitKYC_Click" OnClientClick="return SubmitPayoutKYCDetails();" Text="Submit KYC Details" />
                                    <%--<span id="btnPayoutSubmitKYC" class="btn btn-primary btn-block" style="width: 25%; margin-left: auto; margin-right: auto;" onclick="return SubmitPayoutKYCDetails();">Submit KYC Details</span>--%>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <%}
                else
                { %>

            <%if (IsKycComplete)
                {%>
            <div class="col-sm-12" style="margin-top: 14px; background: #fff; margin-bottom: 14px; padding: 12px;">
                <div class="tab">
                    <a class="tablinks active" onclick="openCity(event, 'user')"><span class="fa fa-user fa-2x text-center"></span>&nbsp;<br />
                        <b>Beneficiary List</b></a>
                    <a class="tablinks" onclick="openCity(event, 'history')" id="TransHistory"><span class="fa fa-history fa-2x"></span>
                        <br />
                        <b>Transaction History</b>
                    </a>
                </div>
                <div id="user" class="tabcontent" style="display: block;">
                    <div class="row">
                        <div class="pull-right">
                            <span id="createReceiver" style="cursor: pointer;" class="btn btn-sm btn-primary" onclick="return AddNewReceiver();">Add Beneficiary</span>
                        </div>
                    </div>
                    <br />
                    <div class="row scrolltable">
                        <table class="table  table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th>Name</th>
                                    <th>Bank</th>
                                    <th>Status</th>
                                    <th>Account No</th>
                                    <th>IFSC Code</th>
                                    <th>Mode</th>
                                    <th style="width: 150px;">Amount</th>
                                    <th>Action</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody id="BeneficaryRowDetails">
                                <%=PayoutRemitterBenDetail %>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div id="history" class="tabcontent">
                    <div class="row">
                        <input type="button" value="Show Filter" class="btn btn-sm btn-primary" id="filterbtn" onclick="ShowHideDiv(this);" />
                    </div>
                    <div class="col-sm-12 form-group filterbox hidden" id="sidebar" style="margin: 10px;">
                        <div class="row form-group">
                            <div class="col-sm-2">
                                <label style="margin-top: 5px;">From Date</label>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control commonDate" placeholder="dd/mm/yyyy" id="txtPayoutTransFromDate" name="txtTransFromDate" style="padding: 4px;" />
                            </div>
                            <div class="col-sm-2">
                                <label style="margin-top: 5px;">To Date</label>
                            </div>
                            <div class="col-sm-4">
                                <input type="text" class="form-control commonDate" placeholder="dd/mm/yyyy" id="txtPayoutTransToDate" name="txtTransToDate" style="padding: 4px;" />
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="col-sm-2">
                                <label style="margin-top: 5px;">Track ID</label>
                            </div>
                            <div class="col-sm-3">
                                <input type="text" name="txtTransTrackId" id="txtPayoutTransTrackId" class="form-control" placeholder="Track ID" style="padding: 4px;" />
                            </div>
                            <div class="col-sm-1">Status</div>
                            <div class="col-sm-3">
                                <select id="ddlPayoutTransStatus" class="form-control" style="height: 36px!important; padding-top: 6px!important;">
                                    <option value="">All</option>
                                    <option value="successful">Success</option>
                                    <option value="under process">Under Process</option>
                                    <option value="failed">Failed</option>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <span class="btn btn-success btn-sm col-sm-7" style="padding: 4px;" id="btnPayoutTransFilter" onclick="PayoutTransFilter();">Search</span>
                                <span class="btn btn-danger btn-sm col-sm-3" style="padding: 4px;" id="btnPayoutClearTransFilter" onclick="PayoutClearTransFilter();">Clear</span>
                            </div>
                        </div>
                    </div>
                    <br />
                    <div class="row scrolltable">
                        <table class="table table-sm table-bordered">
                            <thead>
                                <tr>
                                    <th>Txn Date</th>
                                    <th>Order ID</th>
                                    <th>Track ID</th>
                                    <th>Mode</th>
                                    <th>Amount(₹)</th>
                                    <%--<th>Chg(₹)</th>--%>
                                    <th>Reciver</th>
                                    <th>Status</th>
                                    <th>Refund</th>
                                    <th>Recipt</th>
                                </tr>
                            </thead>
                            <tbody id="PayoutFundTransRowDetails"></tbody>
                        </table>
                    </div>
                </div>
            </div>
            <%}
                else
                { %>
            <div class="col-sm-12" style="margin-top: 14px; background: #fff; margin-bottom: 14px; padding: 12px;">
                <h4 class="text-center" style="color: #ff414d; padding: 80px 80px 10px 80px;">Your KYC documents still pending for approval.</h4>
                <div class="text-center" style="color: #ff414d; padding: 20px 80px 80px 80px; cursor:pointer;">                    
                    <button type="button" class="btn btn-success btn-lg modifykycdetail" data-backdrop="static" data-keyboard="false" data-toggle="modal" data-target="#ModifyKYCDetail"><i class="fa fa-edit"></i>&nbsp;Modify Your Details</button>
                </div>
            </div>
            <%} %>
            <%} %>
        </div>
    </div>

    <uc1:ucdmtdirectfundtransfer runat="server" ID="ucdmtdirectfundtransfer" />

    <script src="custom/js/dmt-direct-pay.js?v=5"></script>
    <script src="custom/js/common.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>

    <script>
        function SubmitPayoutKYCDetails() {
            if (CheckFocusBlankValidation("<%=txtPayoutFirstName.ClientID%>")) return !1;
            if (CheckFocusBlankValidation("<%=txtPayoutLastName.ClientID%>")) return !1;
            if (CheckFocusBlankValidation("<%=txtPayoutAddress.ClientID%>")) return !1;
            if (CheckFocusDropDownBlankValidation("<%=ddlPayoutGender.ClientID%>")) return !1;
            if (CheckFocusBlankValidation("<%=txtPayoutDOB.ClientID%>")) return !1;
            if (CheckFocusDropDownBlankValidation("<%=ddlPayoutProof.ClientID%>")) return !1;
            if (CheckFocusBlankValidation("<%=txtProofNumber.ClientID%>")) return !1;
            var frontimg = $("#<%=fluPayoutFrontImg.ClientID%>").get(0).files.length;
            if (frontimg == 0) { $("#<%=fluPayoutFrontImg.ClientID%>").css("border", "1px solid red").css("padding", "5px"); return false; } else { $("#<%=fluPayoutFrontImg.ClientID%>").removeAttr("style"); }
            var frontimg = $("#<%=fluPayoutBackImg.ClientID%>").get(0).files.length;
            if (frontimg == 0) { $("#<%=fluPayoutBackImg.ClientID%>").css("border", "1px solid red").css("padding", "5px"); return false; } else { $("#<%=fluPayoutBackImg.ClientID%>").removeAttr("style"); }
            if ($('#<%=chkPayoutDeclaration.ClientID%>').is(":checked") == false) { alert("Please check declaration option"); return false; }
        }

        function openCity(evt, cityName) {
            var i, tabcontent, tablinks;
            tabcontent = document.getElementsByClassName("tabcontent");
            for (i = 0; i < tabcontent.length; i++) {
                tabcontent[i].style.display = "none";
            }
            tablinks = document.getElementsByClassName("tablinks");
            for (i = 0; i < tablinks.length; i++) {
                tablinks[i].className = tablinks[i].className.replace(" active", "");
            }
            document.getElementById(cityName).style.display = "block";
            evt.currentTarget.className += " active";
        }
        function ShowHideDiv(btnPassport) {
            var dvPassport = document.getElementById("sidebar");
            if (btnPassport.value == "Show Filter") {
                $(".filterbox").removeClass("hidden")
                btnPassport.value = "Hide Filter";
            } else {
                btnPassport.value = "Hide Filter";
                $(".filterbox").addClass("hidden")
                btnPassport.value = "Show Filter";
            }
        }
        $('.commonDate').datepicker({
            dateFormat: "dd/mm/yy",
            showStatus: true,
            showWeeks: true,
            currentText: 'Now',
            autoSize: true,
            maxDate: -0,
            gotoCurrent: true,
            showAnim: 'blind',
            highlightWeek: true
        });
    </script>
</asp:Content>

