﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using InstantPayServiceLib;

public partial class DMT_Manager_PrintTrans : System.Web.UI.Page
{
    private static string UserId { get; set; }
    private static string Mobile { get; set; }
    private static string RemitterId { get; set; }
    private static string BeneficaryId { get; set; }
    private static string TrackId { get; set; }
    public static string HtmlContent { get; set; }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["UID"] != null && !string.IsNullOrEmpty(Session["UID"].ToString()))
        {
            UserId = Session["UID"].ToString();

            Mobile = Request.QueryString["mobile"] != null ? Request.QueryString["mobile"].ToString() : string.Empty;
            RemitterId = Request.QueryString["sender"] != null ? Request.QueryString["sender"].ToString() : string.Empty;
            BeneficaryId = Request.QueryString["beneficaryid"] != null ? Request.QueryString["beneficaryid"].ToString() : string.Empty;
            TrackId = Request.QueryString["trackid"] != null ? Request.QueryString["trackid"].ToString() : string.Empty;

            if (string.IsNullOrEmpty(Mobile) && string.IsNullOrEmpty(RemitterId) && string.IsNullOrEmpty(BeneficaryId) && string.IsNullOrEmpty(TrackId))
            {
                Response.Redirect("/");
            }
            else
            {
                HtmlContent = BindTransPrint();
            }            
        }
        else
        {
            Response.Redirect("/");
        }
    }

    public string BindTransPrint()
    {
        StringBuilder sbResult = new StringBuilder();

        if (!string.IsNullOrEmpty(UserId))
        {
            if (!string.IsNullOrEmpty(Mobile) && !string.IsNullOrEmpty(RemitterId))
            {
                //DataTable dtAgency = InstantPayFun.GetAgencyDetailById(UserId);

                DataTable dtRemitter = InstantPay_ApiService.GetCombieRemitterDetail(UserId, RemitterId, Mobile);

                sbResult.Append("<div style='border: 1px solid #ff414d; width: 800px; border-collapse: initial;'>");
                sbResult.Append("<div class='col-sm-12'>");
                sbResult.Append("<a href='#'><img src='../Images/gallery/logo(ft).png' style='max-width: 200px;' /></a>");
                sbResult.Append("<span style='float: right!important; text-align: right; padding: 7px;'>");
                sbResult.Append("<span>" + dtRemitter.Rows[0]["FirstName"].ToString() + " " + dtRemitter.Rows[0]["LastName"].ToString() + "</span><br />");
                sbResult.Append("<span>" + dtRemitter.Rows[0]["Address"].ToString() + ", " + dtRemitter.Rows[0]["City"].ToString() + ", " + dtRemitter.Rows[0]["State"].ToString() + ", " + dtRemitter.Rows[0]["PinCode"].ToString() + "</span>");
                sbResult.Append("</span>");
                sbResult.Append("<hr style='text-align: center; border: 1px solid #ff414d; background-color: #ff414d; margin-top: 0px; margin-bottom: 0;' />");
                sbResult.Append("</div>");
                sbResult.Append("<div class='col-sm-12'>");
                sbResult.Append("<h4 style='text-align: center;'>Customer Transaction Receipt</h4>");
                sbResult.Append("<table data-toggle='table' style='width: 800px; border-collapse: initial; border: 1px solid #ff414d; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;'>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Name</th>");
                sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtRemitter.Rows[0]["FirstName"].ToString() + " " + dtRemitter.Rows[0]["LastName"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Sender Mobile Number</th>");
                sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtRemitter.Rows[0]["Mobile"].ToString() + "</td>");
                sbResult.Append("</tr>");

                DataTable dtBen = InstantPay_ApiService.GetBenDetailsByRemitterId(RemitterId, BeneficaryId);

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Name</th>");
                sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["Name"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Account Number</th>");
                sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["Account"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Beneficiary Bank's IFSC</th>");
                sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["IfscCode"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Bank</th>");
                sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtBen.Rows[0]["Bank"].ToString() + "</td>");
                sbResult.Append("</tr>");

                DataTable dtTrans = InstantPay_ApiService.GetTransactionHistoryByTrackId(TrackId);

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Order ID</th>");
                sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["orderid"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction ID</th>");
                sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["ipay_id"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Track ID</th>");
                sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["TrackId"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Amount</th>");
                sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["Amount"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Charged Amount</th>");
                sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["charged_amt"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Date & Times</th>");
                sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + ConvertStringDateToStringDateFormate(dtTrans.Rows[0]["UpdatedDate"].ToString()) + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("<tr style='border-collapse: initial; border: 1px solid rgb(204,204,224); font-size: 11px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<th colspan='2' style='text-align: left; border-right: 1px solid #b3b3b3; border-bottom: 1px solid #b3b3b3; background-color: #f1f1f1; color: #484848; padding: 10px;'>Transaction Status</th>");
                sbResult.Append("<td colspan='2' style='text-align: left; border-bottom: 1px solid #b3b3b3; padding: 10px;'>" + dtTrans.Rows[0]["Status"].ToString() + "</td>");
                sbResult.Append("</tr>");

                sbResult.Append("</table>");
                sbResult.Append("<h4 style='padding-left: 5px;'>Note :-</h4>");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>1. Customer transaction charge is minimum of Rs. 10/- and Maximum 1% of the transaction amount.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>2. In case of non-payment to the beneficiary, the customer will receive an SMS with an OTP that he/she needs to present at the agent location where the transaction was initiated.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>3. In case the Agent charges the Customer in excess of the fee/ charges as mentioned in the receipt, he/she should lodge complaint about the same with our Customer Care on Tel. No. 022 4017 2555 or email us at jaldicash@weizmannimpex.com.</p><br />");
                sbResult.Append("<p style='margin: 0px; padding-left: 5px;'>4. The receipt is subject to terms and conditions, privacy policy and terms of use detailed in the website www.jaldicash.com and shall be binding on the Customer for each transaction.</p><br /><br />");

                sbResult.Append("<table data-toggle='table' style='width: 800px; border-collapse: initial; font-family: Verdana,Geneva,sans-serif; font-size: 12px; border-spacing: 0px; padding: 0px;'>");
                sbResult.Append("<tr>");
                sbResult.Append("<td><p>Date..............................</p></td>");
                sbResult.Append("<td><p style='float: right!important;'>Signature Of Customer's......................................................</p></td>");
                sbResult.Append("</tr>");
                sbResult.Append("</table>");
                sbResult.Append("<br /><br />");
                sbResult.Append("</div>");
                sbResult.Append("</div>");
            }
        }

        return sbResult.ToString();
    }

    public static string ConvertStringDateToStringDateFormate(string date)
    {
        DateTime dtDate = new DateTime();

        if (!string.IsNullOrEmpty(date))
        {
            dtDate = DateTime.Parse(date);
            return dtDate.ToString("dd MMM yyyy hh:mm tt");
        }

        return string.Empty;
    }
}