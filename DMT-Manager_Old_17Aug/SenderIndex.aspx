﻿<%@ Page Title="" Language="C#" MasterPageFile="~/DMT-Manager/MasterPagefor_money-transfer.master" AutoEventWireup="true" CodeFile="SenderIndex.aspx.cs" Inherits="DMT_Manager_dmt_SenderIndex" %>

<%@ Register Src="~/DMT-Manager/User_Control/uc_dmt_model.ascx" TagPrefix="uc1" TagName="uc_dmt_model" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <link href="custom/css/validationcss.css" rel="stylesheet" />

    <section class="hero-wrap section shadow-md py-4">
        <div class="hero-mask opacity-7 bg-dark"></div>
        <div class="hero-bg" style="background-image: url('images/bg/image-6.jpg');"></div>
        <div class="hero-content py-5">
            <div class="container">
                <div class="row">
                    <div class="col-lg-10 col-xl-10 my-auto" style="margin-left: auto; margin-right: auto;">
                        <div class="bg-white rounded shadow-md p-4">
                            <div class="row">
                                <div class="col-sm-9"><h3 class="text-5 text-center">Enter Sender’s Mobile Number</h3></div>
                                <div class="col-sm-3"><span id="btnNewRegistration" style="cursor: pointer;" class="btn btn-sm btn-primary btn-block" onclick="NewRegistration();">New Registration</span></div>
                            </div>
                            <hr class="mb-4" />
                            <div class="row">
                                <div class="col-sm-9 col-lg-offcet-3" style="margin-left: auto; margin: auto;">
                                    <div class="row">
                                        <div class="col-lg-8 form-validation">
                                            <input type="text" id="txtSenderMobileNo" value="" class="form-control sendmobileno" placeholder="Sender Mobile No." maxlength="10" autocomplete="off" onkeypress="return isNumberValidationPrevent(event);" />
                                            <p style="color: #ccc;">(10 digits) Please don not use prefix zero (0)</p>
                                        </div>
                                        <div class="col-lg-4">
                                            <span id="btnRemitterMobileSearch" style="cursor: pointer;" class="btn btn-primary btn-block" onclick="RemitterMobileSearch();">Search</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-xl-6 my-auto" id="RemitterDetailsByMobile"></div>
                </div>
            </div>
        </div>
        <div class="hero-content py-5">
            <div class="container">
                <div class="col-sm-12">
                    <div class="row">
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br />
                                <h3 class="text-5 text-center">Step 1</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Register a Sender</h3>
                                <p>Fill in basic information and register a Sender for Money Transfer. Upgrade Sender to KYC by uploading a Photo Id and an Address Proof.</p>
                                <%-- <p>मूलभूत जानकारी भरकर प्रेषक को पैसा भेजने के लिए पंजीकृत करें | एक फोटो आई डी और पता प्रमाण अपलोड कर प्रेषक की जानकारीअपग्रेड करें।</p>--%>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br />
                                <h3 class="text-5 text-center">Step 2</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Add a Beneficiary</h3>
                                <p>Add multiple Beneficiaries / Receivers against each Sender.</p>
                                <%--<p>प्रत्येक प्रेषक के अन्तर्गत विभिन्न लाभार्थीं / प्राप्तकर्ता जोड़ सकते हैं।</p>--%>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="col-sm-12" style="border-radius: 4px; background: #fff; box-shadow: 0px 0px 4px #000000; min-height: 240px;">
                                <br />
                                <h3 class="text-5 text-center">Step 3</h3>
                                <h3 class="text-5 text-center" style="font-size: 18px !important; border-bottom: 1px solid #000;">Transfer Money</h3>
                                <p>Instantly begin transferring money.</p>
                                <%--<p>तुरंत पैसा भेजने की प्रक्रिया शुरू करें।</p>--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
        
    <uc1:uc_dmt_model runat="server" ID="uc_dmt_model" />

    <script src="custom/js/new_dmt.js"></script>
    <script src="custom/js/common.js"></script>
</asp:Content>

